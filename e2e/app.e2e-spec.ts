import { RxExamplesPage } from './app.po';

describe('rx-examples App', () => {
  let page: RxExamplesPage;

  beforeEach(() => {
    page = new RxExamplesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
