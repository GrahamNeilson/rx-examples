import { Component, OnInit, ViewChild } from '@angular/core';

import { Observable } from '../app.rx';

@ViewChild('container')
@Component({
  selector: 'app-drag',
  styles: [`
    .container {
      height: 200px;
      background-color: Orange;
      transition: all;
    }
  `],
  templateUrl: './drag.component.html',
})
export class DragComponent implements OnInit {

  mousemove$: Observable<MouseEvent>;
  mousedrag$: Observable<MouseEvent>;
  mouseup$: Observable<MouseEvent>;
  mousedown$: Observable<MouseEvent>;

  width: Observable<number>;
  direction: Observable<string>;

  ngOnInit() {
    this.mousemove$ = Observable.fromEvent(document, 'mousemove');
    this.mouseup$ = Observable.fromEvent(document, 'mouseup');
    this.mousedown$ = Observable.fromEvent(document, 'mousedown');

    this.mousedrag$
      = this.mousedown$
          .switchMap(() => {
            return this.mousemove$
                      .takeUntil(this.mouseup$);
          });

    this.width
      = this.mousedrag$
        .map(mousedrag => mousedrag.clientX);

    this.direction =
      this.mousedrag$
          .pairwise()
          .map(values => {
            const direction = values[1].clientX - values[0].clientX;
            return direction < 0 ? 'contract' : 'expand';
          })
          .startWith('None');
  }
}
