import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Jsonp, URLSearchParams, Response} from '@angular/http';
import { Observable, Subject } from '../app.rx';

@Component({
  selector: 'app-typeahead',
  templateUrl: './typeahead.component.html',
})
export class TypeaheadComponent {

  userInput = new FormControl();

  searchResults: Observable<string[]> =
    this.userInput.valueChanges
        .debounce(() => Observable.interval(250))
        .distinctUntilChanged()
        .map(this.transformSearchTermToSearchParams)
        .switchMap((search: URLSearchParams) => {
          return this.jsonp
              .get('http://en.wikipedia.org/w/api.php', { search })
              .retry(3)
              .map((response: Response) => response.json()[1]);
        });


  // need jsonp injecte
  constructor(private jsonp: Jsonp) {}

  transformSearchTermToSearchParams(searchTerm: string): URLSearchParams {
    const params = new URLSearchParams();
    params.set('action', 'opensearch');
    params.set('format', 'json');
    params.set('callback', 'JSONP_CALLBACK');
    params.set('search', searchTerm);

    return params;
  }
}
