import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes, provideRoutes } from '@angular/router';

import { AppComponent } from './app.component';
import { DragComponent } from './drag/drag.component';
import { TypeaheadComponent } from './typeahead/typeahead.component';

const appRoutes: Routes = [
  { path: 'typeahead', component: TypeaheadComponent },
  { path: 'drag',      component: DragComponent },
  { path: '',
    redirectTo: '/typeahead',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    TypeaheadComponent,
    DragComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
